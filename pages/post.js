import { useRouter } from 'next/router';
import Layout from '../comps/layout';

const BluePage = () => {
    const router = new useRouter();
    return(
        <Layout>
            <h3>{router.query.title}</h3>
            <p>The content of the page goes here</p>
        </Layout>
    )
}

export default BluePage;