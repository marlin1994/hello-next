import { useRouter } from 'next/router';
import Layout from '../../comps/layout';
import fetch from 'isomorphic-unfetch';

// const BluePage = () => {
//     const router = new useRouter();
//     return(
//         <Layout>
//             <h3>{router.query.id}</h3>
//             <p>The content of the page goes here</p>
//         </Layout>
//     )
// }

// export default BluePage;

const Post = props => (
  <Layout>
    <h1>{props.show.name}</h1>
    <p>{props.show.summary.replace(/<[/]?[pb]>/g, '')}</p>
    {props.show.image ? <img src={props.show.image.medium} /> : null}
  </Layout>
);

Post.getInitialProps = async function(context) {
  const { id } = context.query;
  const res = await fetch(`https://api.tvmaze.com/shows/${id}`);
  const show = await res.json();

  console.log(`Fetched show: ${show.name}`);

  return { show };
};

export default Post;