import Layout from '../comps/layout';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';

// const Some = props => (
//     // <li>
//     //     <Link href={`/post?title=${props.title}`}>
//     //         <a>{props.title}</a>
//     //     </Link>
//     // </li>
//     <li>
//         <Link href='/p/[id]' as={`/p/${props.id}`}>
//             <a>{props.id}</a>
//         </Link>
//     </li>
// )

// export default function Index() {
//     return (
//         // <Layout>
//         //     <h1>My Blog</h1>
//         //     <Some title='Hello World 1!' />
//         //     <Some title='Hello World 2!' />
//         //     <Some title='Hello World 3!' />
//         // </Layout>
//         <Layout>
//             <h1>My Blog</h1>
//             <Some id='Hello World 1!' />
//             <Some id='Hello World 2!' />
//             <Some id='Hello World 3!' />
//         </Layout>
//     )
// }


const Index = props => (
  <Layout>
    <h1>Batman TV Shows</h1>
    <ul>
      {props.shows.map(show => (
        <li key={show.id}>
          <Link href="/p/[id]" as={`/p/${show.id}`}>
            <a>{show.name}</a>
          </Link>
        </li>
      ))}
    </ul>
  </Layout>
);

Index.getInitialProps = async function() {
  const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
  const data = await res.json();

  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    shows: data.map(entry => entry.show)
  };
};

export default Index;