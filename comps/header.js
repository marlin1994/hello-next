import Link from 'next/link';

const headStyle = {
    marginRight: 15
}

export default function Header() {
    return (
        <div>
            <Link href='/'>
                <a style={headStyle}>Home</a>
            </Link>
            <Link href='/about'>
                <a style={headStyle}>About</a>
            </Link>
        </div>
    );
}